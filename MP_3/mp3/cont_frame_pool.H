/*
 File: cont_frame_pool.H
 
 Author: R. Bettati
 Department of Computer Science
 Texas A&M University
 Date  : 17/02/04 
 
 Description: Management of the CONTIGUOUS Free-Frame Pool.
 
 As opposed to a non-contiguous free-frame pool, here we can allocate
 a sequence of CONTIGUOUS frames.
 
 */

#ifndef _CONT_FRAME_POOL_H_                   // include file only once
#define _CONT_FRAME_POOL_H_

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include "machine.H"
/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* C o n t F r a m e   P o o l  */
/*--------------------------------------------------------------------------*/

#define FREE_STATE 0b11000000
#define ALLOC_STATE 0b00000000
#define HS_STATE 0b01000000
#define MK_INACCESIBLE 0b1000000

class ContFramePool {
    
private:
    /* -- DEFINE YOUR CONT FRAME POOL DATA STRUCTURE(s) HERE. */
    
    //Working 2-bit per frame, hence a char can hold ->4Frames information.
    //Basic Algorithm for working with other parameters
    // 00->occupied,01->HeadOfSeq, 11->FreeFrame.
    unsigned char * bitmap; //Points to the address for a given byte. eg it shows address for 0x1000 means 1KB.and similar.
    unsigned int nfree_frames;//Initial Checking to decide if there are free frames available.
    unsigned long base_frame_no;//The starting frame number out of 8K frames.
    unsigned long n_frames;//No of frames in the sizepool.
    unsigned long info_frame_no;//THis Frame holds the management Information.(Starting Frame Number).
    unsigned no_of_frames_bitmap_index;
    
    static ContFramePool* frame_pool_list;//class constant
    ContFramePool* prev;
	ContFramePool* next;
	//Lets make it simple, don't implement the release_frames function(HS_frame_number) as a new Function\
	try asserting with assert(Base_frameNO<release_frameNumber<BaseFrameNo+N_frames),such that we can prevent \
	corrupting other frame pools memory.
	
	//Algorithm for bitmap Manipulation:-
		
		/*At any Constructor or Destructor aim is to provide only check,release and get frameDetails synchronously.
		 * Testing Code must be writter properly and precisely as needed.
		 * 
		 * 
		 * Initialization Algorithms-> for Base_FrameNO -> Will be the one that is starting free for allocation.
		 * The initial info_frames are not taken into consideration, And This BaseFrame can as well be used for release
		 * assertion.
		 * 
		 * Algorithm:
		 * 
		 * 32MB -Page(4KB) 8K Pages.
		 * 
		 * 0th - 4MB Kernel Code:
		 * 			0-2MB Notouch
		 * 			2-4MB Frame,KernelCode,stack
		 * 4MB-32MB - Process(15MB-16MB)
		 * 
		 * bitmap 
		 * char* address =  7-26(20Frames)
		 * 
		 * processPool1 - Baseframe = 7, noframes = 20 
		 * 
		 * 40 bits - char array.
		 * 
	*/
public:

    // The frame size is the same as the page size, duh...    
    static const unsigned int FRAME_SIZE = Machine::PAGE_SIZE; 
	
	void SetFrameState(unsigned long,unsigned char);
	bool CheckFrameState(unsigned long,unsigned char); //Ret T or F by checking corpnd state of frame.

    
    ContFramePool(unsigned long _base_frame_no,
                  unsigned long _n_frames,
                  unsigned long _info_frame_no,
                  unsigned long _n_info_frames);
    /*
     Initializes the data structures needed for the management of this
     frame pool.
     _base_frame_no: Number of first frame managed by this frame pool.
     _n_frames: Size, in frames, of this frame pool.
     
     EXAMPLE: If _base_frame_no is 16 and _n_frames is 4, this frame pool manages
     physical frames numbered 16, 17, 18 and 19.
     _info_frame_no: Number of the first frame that should be used to store the
     management information for the frame pool.
     
     NOTE: If _info_frame_no is 0, the frame pool is free to
     choose any frames from the pool to store management information.
     _n_info_frames: If _info_frame_no is 0, this argument specifies the
     number of consecutive frames needed to store the management information
     for the frame pool.
     
     EXAMPLE: If _info_frame_no is 699 and _n_info_frames is 3,
     then Frames 699, 700, and 701 are used to store the management information
     for the frame pool.
     NOTE: This function must be called before the paging system
     is initialized.
     */
    
    unsigned long get_frames(unsigned int _n_frames);
    /*
     Allocates a number of contiguous frames from the frame pool.
     _n_frames: Size of contiguous physical memory to allocate,
     in number of frames.
     If successful, returns the frame number of the first frame.
     If fails, returns 0.//Obviously base frame number is never going to 0, since it used in kernel code.
     */
    
    void mark_inaccessible(unsigned long _base_frame_no,
                           unsigned long _n_frames);
    /*
     Marks a contiguous area of physical memory, i.e., a contiguous
     sequence of frames, as inaccessible.
     _base_frame_no: Number of first frame to mark as inaccessible.
     _n_frames: Number of contiguous frames to mark as inaccessible.
     */
    
    static void release_frames(unsigned long _first_frame_no);
    /*
     Releases a previously allocated contiguous sequence of frames
     back to its frame pool.
     The frame sequence is identified by the number of the first frame.
     NOTE: This function is static because there may be more than one frame pool
     defined in the system, and it is unclear which one this frame belongs to.
     This function must first identify the correct frame pool and then call the frame
     pool's release_frame function.
     */
    
    static unsigned long needed_info_frames(unsigned long _n_frames);
    /*
     Returns the number of frames needed to manage a frame pool of size _n_frames.
     The number returned here depends on the implementation of the frame pool and 
     on the frame size.
     EXAMPLE: For FRAME_SIZE = 4096 and a bitmap with a single bit per frame 
     (not appropriate for contiguous allocation) one would need one frame to manage a 
     frame pool with up to 8 * 4096 = 32k frames = 128MB of memory!
     This function would therefore return the following value:
       _n_frames / 32k + (_n_frames % 32k > 0 ? 1 : 0) (always round up!)
     Other implementations need a different number of info frames.
     The exact number is computed in this function..
     */
};
#endif
