/* 
    Author: R. Bettati, Joshua Capehart
            Department of Computer Science
            Texas A&M University
			
	    A thread scheduler.

*/
#ifndef SCHEDULER_H
#define SCHEDULER_H

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include "thread.H"
#include "simple_timer.H"
#include "console.H"

/*--------------------------------------------------------------------------*/
/* !!! IMPLEMENTATION HINT !!! */
/*--------------------------------------------------------------------------*/
/*
    One way to proceed is to implement the FIFO scheduling policy inside
    class 'Scheduler'. 

    If you plan to implement a Round-Robin Scheduler, derive it from class
    'Scheduler', say as class 'RRScheduler'. The class 'RRScheduler' is 
    really just a FIFO scheduler with THREE MODIFICATIONS:
    1. It manages a timer, which fires at the end-of-quantum (EOQ). 
    (For details on how to set up a timer and how to handle timer interrupts 
    see the 1-second timer in 'kernel.C'.)  The timer is set up in the
    constructor.
    2. It uses an additional function, the EOQ handler. This function gets
    called whenever an EOQ timer event fires. The EOQ handler forces the 
    current thread to call the scheduler's 'yield' function.
    3. The 'yield' function must be modified to account for unused quantum
    time. If a thread voluntarily yields, the EOQ timer must be reset in order
    to not penalize the next thread.
 
    (Note that this qualifies as programming at about the level of a baboon.
     Much better would be to have the abstract class 'Scheduler' implement 
     the basic scheduling MECHANISMS and provide abstract funtions to define
     the queue management POLICIES in derived classes, 
     such as 'FIFOScheduler'.)
     //Required To study Derived class first.
    
 */
/* ----------------------- QUEUE -------------------------------------------*/
class Queue //Dynamic Queue Operations.
{	//Using a linkedlist based dynamic Queue Data Structure
	//Variables are initialized At the constructor level.
	private:
	struct threadData
	{
		Thread * threadPtr;
		int threadID;
		threadData* next;
		threadData* prev;
	};
	unsigned int size;//Returns the current size of queue.
	threadData* front_of_queue_ptr;//Elements are removed from here
	threadData* rear_of_queue_ptr;//Elements are pushed in here.
	
	public:
	Queue();
	Thread* Dequeue();//Ret ptr of front thread and remove it from queue
	void Enqueue(Thread*,int);//Add this to thread to read_end_of queue.
	Thread* Front();//Ret ptr to front element of thread,doesnt remove it.
	Thread* Rear();//ret last element of queue,doesnt remove it.
	void Remove(Thread*);//Remove element from queue with matching threadPtr.
	bool IsEmpty();//Says if the thread is empty or not.(using size).
};
/*--------------------------------------------------------------------------*/
/* SCHEDULER */
/*--------------------------------------------------------------------------*/
class Scheduler;

class quantumClass : public SimpleTimer
{
private:
	int ticks_quantumClass;
	Scheduler *tq_Scheduler;
public:
	quantumClass(int _hz,Scheduler* call_scheduler): SimpleTimer(_hz)
	{
		this->tq_Scheduler=call_scheduler;
		this->ticks_quantumClass=0;
		Console::puts("Quantum Class Created\n");
	}
	
	void handle_interrupt(REGS*);
};

class Scheduler {
	
  /* The scheduler may need private members... */
public:
	//Initialization and bookkeeping variables.
	unsigned int numOfThreads;
	//Used for storing the current queue to hold the threads
	Queue* schedulerQueue;
	Thread* CurrentThread;
	bool roundRobin_Enable;
	
   Scheduler();
   /* Setup the scheduler. This sets up the ready queue, for example.
      If the scheduler implements some sort of round-robin scheme, then the 
      end_of_quantum handler is installed in the constructor as well. */

   /* NOTE: We are making all functions virtual. This may come in handy when
            you want to derive RRScheduler from this class. */
   virtual void yield();
   /* Called by the currently running thread in order to give up the CPU. 
      The scheduler selects the next thread from the ready queue to load onto 
	  the CPU, and calls the dispatcher function defined in 'Thread.H' to
      do the context switch. */
	//The yielding function is not put into t
   virtual void resume(Thread * _thread);
   /* Add the given thread to the ready queue of the scheduler. This is called
      for threads that were waiting for an event to happen, or that have 
      to give up the CPU in response to a preemption. */
		
   virtual void add(Thread * _thread);
   /* Make the given thread runnable by the scheduler. This function is called
      after thread creation. Depending on implementation, this function may 
      just add the thread to the ready queue, using 'resume'. */

   virtual void terminate(Thread * _thread);
   /* Remove the given thread from the scheduler in preparation for destruction
      of the thread. 
      Graciously handle the case where the thread wants to terminate itself.*/
  
};
	


	
class RRScheduler:public Scheduler
{
	/*
	 * RRschduler is a derived class from Scheduler, everything is almost
	 * identical except to the yield() function, which requires slight
	 * modifications
	 */ 
	private:
	/*
	 * timer_quantum holds the address of simpleTimer derived quantum
	 * class pointer,the timer and interrupt handlers are all part
	 * of the quantumclass to enable RRScheduler function
	 */ 
		quantumClass * timer_quantum; //Initialize the Handler with EOQ
		
	public:
		/*
		 * Initializes the variables, and then creates the quantumclass
		 * to intialize interrupt handler and then register the 
		 * interrupt handler.
		 */ 
		RRScheduler();
		/*
		 * Almost similar to Scheduler::yield(), except for checking
		 * null threads.
		 */ 
		void yield();
	
};	

#endif
	
